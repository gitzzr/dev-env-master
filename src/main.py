# coding:utf-8
import sys

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QApplication

from app.view.mainWindow import MainWindow


# 启用dpi刻度
# if cfg.get(cfg.dpiScale) == "Auto":
#     QApplication.setHighDpiScaleFactorRoundingPolicy(
#         Qt.HighDpiScaleFactorRoundingPolicy.PassThrough)
#     QApplication.setAttribute(Qt.AA_EnableHighDpiScaling)
# else:
#     os.environ["QT_ENABLE_HIGHDPI_SCALING"] = "0"
#     os.environ["QT_SCALE_FACTOR"] = str(cfg.get(cfg.dpiScale))

QApplication.setAttribute(Qt.AA_UseHighDpiPixmaps)

# create application
app = QApplication(sys.argv)
app.setAttribute(Qt.AA_DontCreateNativeWidgetSiblings)

# 国际化
# locale = cfg.get(cfg.language).value
# translator = FluentTranslator(locale)
# galleryTranslator = QTranslator()
# galleryTranslator.load(locale, "gallery", ".", ":/gallery/i18n")
#
# app.installTranslator(translator)
# app.installTranslator(galleryTranslator)

# create main window
w = MainWindow()
w.show()

app.exec_()

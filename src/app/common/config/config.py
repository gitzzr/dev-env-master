import json
import os


def getConfigFile():
    """获取配置文件路径"""
    return os.path.join(os.path.dirname(__file__), 'config.json')


def getConfig() -> dict:
    # 获取配置数据
    with open(getConfigFile(), 'r', encoding='utf-8') as f:
        try:
            config = json.load(f)
            return config
        except Exception as e:
            print(e)
            return {}


def getDevLanguageEnvConfig() -> dict:
    # 获取DevLanguageEnvConfig配置数据
    config = getConfig()
    try:
        return config['DevLanguageEnvConfig']
    except KeyError:
        return []


def setDevLanguageEnvConfig(devLanguageEnvConfig):
    # 将DevLanguageEnvConfig配置数据写入配置文件
    config = getConfig()
    config['DevLanguageEnvConfig'] = devLanguageEnvConfig
    with open(getConfigFile(), "w") as f:
        # print("写入配置文件：", config)
        json.dump(config, f, indent=4, ensure_ascii=False)  # indent表示缩进空格数 ensure_ascii=False表示不转换为ascii码


AUTHOR = "zhanzr"
VERSION = "1.0.0"
APP_NAME_CN = "开发环境大师"
APP_NAME_EN = "DevEnvMaster"
REPOSITORY_URL = ""
SUPPORT_URL = "https://mp-fdc42696-297b-49e2-b9b5-b5c447cedc75.cdn.bspapp.com/wxzf.jpg"


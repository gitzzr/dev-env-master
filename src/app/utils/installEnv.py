import os

from ..common.config.config import getDevLanguageEnvConfig
from ..models.envVariables import EnvVariables


def install_env(env_name: str, is_all_user: bool = True):
    """
    安装JDK环境变量。
    """
    dev_language = None
    for item in getDevLanguageEnvConfig():
        if item['name'].lower() == env_name.lower():
            dev_language = item
            break
    if dev_language is None:
        print(f"未找到 {env_name} 语言所对应的配置")
        raise Exception(f"未找到 {env_name} 语言所对应的配置")

    env_var_name = dev_language['env_var_name']
    env_var_value = dev_language['env_var_value']
    path_env_var_suffix = dev_language['path_env_var_suffix']

    # print(f'开始配置{dev_language["name"]}的环境变量,'
    #       f'{env_var_name}的路径为：{env_var_value}')

    # 需要新增到path变量的值
    path_add_list = []
    for item in path_env_var_suffix:
        path_add_list.append(os.path.join(f'%{env_var_name}%', item))

    # 更新环境变量
    env_variables = EnvVariables(is_all_user)
    env_variables.create_variable(env_var_name, env_var_value)
    env_variables.add_path_value(path_add_list)

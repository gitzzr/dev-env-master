from PyQt5.QtCore import Qt, QRunnable, QThreadPool, pyqtSignal
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QHBoxLayout
from qfluentwidgets import ScrollArea, CaptionLabel, \
    PushButton, FluentIcon, TitleLabel

from ..common.style_sheet import StyleSheet
from ..components.devLanguageItem import DevLanguageItem
from ..models.devLanguage import getAllDevLanguage


# 定义一个可运行的任务类，用于在后台线程中执行addCard操作
class AddCardTask(QRunnable):
    def __init__(self, devLanguage, devLanguageInterface):
        super().__init__()
        self.devLanguage = devLanguage
        self.devLanguageInterface = devLanguageInterface

    def run(self):
        # 发送信号，通知主线程添加组件
        self.devLanguageInterface.addCardSignal.emit(self.devLanguage)


class ToolBar(QWidget):
    """ Tool bar """

    def __init__(self, title, subtitle, parent=None):
        super().__init__(parent=parent)
        self.titleLabel = TitleLabel(title, self)
        self.subtitleLabel = CaptionLabel(subtitle, self)

        # self.addDevLanguageButton = PushButton(self.tr('本地添加语言'), self, FluentIcon.DOCUMENT)

        self.vBoxLayout = QVBoxLayout(self)
        self.buttonLayout = QHBoxLayout()

        self.__initWidget()

    def __initWidget(self):
        self.setFixedHeight(108)
        self.vBoxLayout.setSpacing(0)
        self.vBoxLayout.setContentsMargins(36, 22, 36, 12)
        self.vBoxLayout.addWidget(self.titleLabel)
        self.vBoxLayout.addSpacing(4)
        self.vBoxLayout.addWidget(self.subtitleLabel)
        self.vBoxLayout.addSpacing(4)
        self.vBoxLayout.addLayout(self.buttonLayout, 1)
        self.vBoxLayout.setAlignment(Qt.AlignTop)

        self.buttonLayout.setSpacing(4)
        self.buttonLayout.setContentsMargins(0, 0, 0, 0)
        # self.buttonLayout.addWidget(self.addDevLanguageButton, 0, Qt.AlignLeft)
        # self.addDevLanguageButton.clicked.connect(self.__onAddDevLanguage)

    def __onAddDevLanguage(self):
        pass


class DevLanguageInterface(ScrollArea):
    # 定义添加组件的信号
    addCardSignal = pyqtSignal(object)

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.view = QWidget()
        self.toolBar = ToolBar("开发语言目录", "更多语言敬请期待...", self)
        self.vBoxLayout = QVBoxLayout(self.view)
        # self.addCardSignal.connect(self.addCard)
        for devLanguage in getAllDevLanguage():
            self.addCard(devLanguage)
        # 初始化界面
        self.__initWidget()

    def __initWidget(self):
        self.resize(900, 700)  # 设置窗口大小
        self.setMinimumSize(600, 400)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)  # 隐藏水平滚动条
        self.setViewportMargins(0, self.toolBar.height(), 0, 0)  # 设置滚动区域
        self.setWidget(self.view)  # 设置滚动区域的窗口部件
        self.setWidgetResizable(True)  # 设置滚动区域窗口部件自适应
        self.setObjectName('devLanguageInterface')  # 设置对象名称
        self.view.setObjectName('view')

        # 初始化界面样式
        # 1. 通过StyleSheet加载样式
        StyleSheet.DEV_LANGUAGE_INTERFACE.apply(self)
        # 2. 通过qrc文件加载样式
        # style_file = QFile(':/devEnvMaster/qss/light/dev_language_interface.qss')
        # style_file.open(QFile.ReadOnly | QFile.Text)
        # style_str = str(style_file.readAll(), encoding='utf-8')
        # self.setStyleSheet(style_str)

        # 初始化布局
        self.__initLayout()

    def __initLayout(self):
        self.vBoxLayout.setSpacing(10)  # 设置布局内部控件间距
        self.vBoxLayout.setAlignment(Qt.AlignTop)  # 设置布局内部控件对齐方式
        self.vBoxLayout.setContentsMargins(20, 20, 20, 20)  # 设置布局外边距

    def addCard(self, devLanguage):
        appCard = DevLanguageItem(devLanguage, self)
        self.vBoxLayout.addWidget(appCard, 0, Qt.AlignTop)

    def addCardInBackground(self, devLanguage):
        task = AddCardTask(devLanguage, self)
        QThreadPool.globalInstance().start(task)

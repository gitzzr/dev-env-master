# coding:utf-8
import sys
import time

from PyQt5.QtCore import QUrl
from PyQt5.QtGui import QIcon, QDesktopServices
from PyQt5.QtWidgets import QApplication
from qfluentwidgets import FluentIcon as FIF, NavigationAvatarWidget, MessageBox
from qfluentwidgets import FluentWindow, NavigationItemPosition

from ..common.config.config import SUPPORT_URL
from .devLanguageInterface import DevLanguageInterface
from ..common import resource_rc


class MainWindow(FluentWindow):

    def __init__(self):
        super().__init__()
        self.initWindow()

        # 创建DevLanguageInterface子界面
        start_time = time.time()
        self.languageInterface = DevLanguageInterface(self)
        end_time = time.time()
        creation_time = end_time - start_time
        print(creation_time)
        print(f"DevLanguageInterface创建时间：{creation_time:.2f}秒")

        # 初始化导航栏,将子界面添加到导航栏
        self.__init_navigation()

    def __init_navigation(self):
        self.addSubInterface(
            self.languageInterface,
            FIF.APPLICATION,
            "开发语言",
            position=NavigationItemPosition.TOP
        )
        # add custom widget to bottom
        self.navigationInterface.addWidget(
            routeKey='avatar',
            widget=NavigationAvatarWidget('zhanzr', ':/devEnvMaster/images/logo.png'),
            onClick=self.onSupport,
            position=NavigationItemPosition.BOTTOM
        )

    def initWindow(self):
        # 设置窗口大小
        self.resize(900, 700)
        # 设置窗口图标
        self.setWindowIcon(QIcon(":/devEnvMaster/images/logo.png"))
        # 设置窗口标题
        self.setWindowTitle('开发环境大师')
        # 设置窗口居中
        desktop = QApplication.desktop().availableGeometry()
        width, height = desktop.width(), desktop.height()
        self.move(width//2 - self.width()//2, height//2 - self.height()//2)
        # 显示窗口
        self.show()
        # 使窗口立即显示
        QApplication.processEvents()

    def onSupport(self):
        w = MessageBox(
            '支持作者🥰',
            '个人开发不易，如果这个项目帮助到了您，可以考虑请作者喝一瓶快乐水🥤。您的支持就是作者开发和维护项目的动力🚀',
            self
        )
        w.yesButton.setText('来啦王子/公主')
        w.cancelButton.setText('下次一定')
        if w.exec():
            QDesktopServices.openUrl(QUrl(SUPPORT_URL))
            pass


if __name__ == '__main__':
    # enable dpi scale
    # QApplication.setHighDpiScaleFactorRoundingPolicy(Qt.HighDpiScaleFactorRoundingPolicy.PassThrough)
    # QApplication.setAttribute(Qt.AA_EnableHighDpiScaling)
    # QApplication.setAttribute(Qt.AA_UseHighDpiPixmaps)
    app = QApplication(sys.argv)
    w = MainWindow()
    w.show()
    app.exec()

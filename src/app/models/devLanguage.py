import os

from ..common.config.config import getDevLanguageEnvConfig, setDevLanguageEnvConfig
from ..utils.checkDevLanguageVersion import checkVersionForDevLanguage
from ..utils.installEnv import install_env


# 通用安装环境方法
def install_common_env(self):
    try:
        invalid_path = self.checkEnvPathValid()
        if invalid_path:
            raise FileNotFoundError(f"该路径下不存在 {invalid_path} 目录")
        install_env(self.name, True)
        return True
    except Exception as e:
        return False


def install_java_env(self):
    return install_common_env(self)


def install_maven_env(self):
    return install_common_env(self)


def install_python_env(self):
    pass


def install_node_env(self):
    pass


class DevLanguage:
    """DevLanguage class"""

    def __init__(self, name, icon, env_var_name, env_var_value, path_env_var_suffix, version_command, version_regex,
                 version_verify_suffix):
        self.name = name
        self.version = ""
        self.icon = f":/devEnvMaster/images/dev_language/{icon}"
        self.env_var_name = env_var_name
        self.env_var_value = env_var_value
        self.path_env_var_suffix = path_env_var_suffix
        self.version_command = version_command
        self.version_regex = version_regex
        self.version_verify_suffix = version_verify_suffix
        self.installFunction = "install_" + self.name.lower() + "_env"

    def __str__(self):
        return f"DevLanguage(name={self.name}, version={self.version}, icon={self.icon}, " \
               f"env_var_name={self.env_var_name}, env_var_value={self.env_var_value}, " \
               f"version_command={self.version_command}, installFunction={self.installFunction})"

    def setEnvValToConfig(self):
        """ 将环境变量值写入配置文件 """
        __devLanguageEnvConfig = getDevLanguageEnvConfig()
        for i in range(len(__devLanguageEnvConfig)):
            if __devLanguageEnvConfig[i]["name"].lower() == self.name.lower():
                __devLanguageEnvConfig[i]["env_var_value"] = self.env_var_value
                break
        setDevLanguageEnvConfig(__devLanguageEnvConfig)

    def getCurrentConfigEnvVersion(self):
        """ 获取当前配置文件的环境变量版本 """
        try:
            return checkVersionForDevLanguage(self.name)
        except Exception as e:
            return ""

    def getVersion(self):
        """ 获取版本 """
        return checkVersionForDevLanguage(self.name, self.env_var_value)

    def checkIsSameLanguage(self, path):
        """ 校验是否同一种语言 """
        try:
            return checkVersionForDevLanguage(self.name, path)
        except Exception:
            return None

    def install_env(self):
        """ 安装环境 """
        return eval(self.installFunction)(self)

    def checkEnvPathValid(self):
        """ 校验环境路径是否有效 """
        invalid_path_list = []
        for item in self.path_env_var_suffix:
            path_value = os.path.join(self.env_var_value, item)
            print("检查路径：", path_value)
            if not os.path.exists(path_value):
                invalid_path_list.append(item)
        if len(invalid_path_list) > 0:
            return "、".join(invalid_path_list)
        return None


def getAllDevLanguage():
    __devLanguageEnvConfig = getDevLanguageEnvConfig()
    __devLanguageList = []
    for devLanguage in __devLanguageEnvConfig:
        __devLanguageList.append(
            DevLanguage(
                name=devLanguage["name"],
                icon=devLanguage["icon"],
                env_var_name=devLanguage["env_var_name"],
                env_var_value=devLanguage["env_var_value"],
                path_env_var_suffix=devLanguage["path_env_var_suffix"],
                version_command=devLanguage["version_command"],
                version_regex=devLanguage["version_regex"],
                version_verify_suffix=devLanguage["version_verify_suffix"]
            )
        )
    return __devLanguageList
